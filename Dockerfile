FROM ubuntu AS builder

ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Paris"

RUN apt update && apt install -y git make pkg-config gcc libz-dev \
    && git clone https://github.com/yarrick/iodine.git \
    && cd /iodine && make && mkdir -p /output && mv /iodine/bin/* /output/ 

ADD start.sh /output/start-iodined

RUN chmod +x /output/start-iodined

#===============

FROM alpine

RUN apk --no-cache add gcompat iptables

COPY --from=builder /output/ /bin/

EXPOSE 53/udp

CMD ["/bin/sh","/bin/start-iodined"]
